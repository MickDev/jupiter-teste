# Site

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.0.

To run this project you need to run two commands:
1) json-server --watch db.json  (this command will initialise the fake-database)
2) ng serve (this command startup the project)